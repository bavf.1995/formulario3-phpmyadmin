<?php

public class Comuna
{
    private $id_region;
    private $id_provincia;
    private $id_comuna;
    private $nombre;

    public function getId_region()
    {
        return $this->id_region;
    }
    public function setId_region($id_region)
    {
        $this->id_region = $id_region;
    }

    public function getId_provincia()
    {
        return $this->id_provincia;
    }
    public function setId_provincia($id_provincia)
    {
        $this->id_provincia = $id_provincia;
    }

    public function getId_comuna()
    {
        return $this->id_comuna;
    }
    public function setId_comuna($id_comuna)
    {
        $this->id_comuna = $id_comuna;
    }

    public function getNombre()
    {
        return $this->nombre;
    }
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }
}
