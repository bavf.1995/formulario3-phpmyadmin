<!DOCTYPE html>
<html>
<!--Cabecera de la pagina-->
<head>
	<!--Pie de Pagina-->
	<title>Prueba dai</title>
	<!--Tipo de Codificacion que se utilizara en nuestra Pagina-->
	<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script type="text/javascript" src="js/dv.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/localidades.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<div class="jumbotron">
				<div id="demo" class="carousel slide" data-ride="carousel">

						<!-- Indicador -->
						<ul class="carousel-indicators">
							<li data-target="#demo" data-slide-to="0" class="active"></li>
							<li data-target="#demo" data-slide-to="1"></li>
							<li data-target="#demo" data-slide-to="2"></li>
						</ul>
					
						<!-- El Presentador de Imagenes -->
						<div class="carousel-inner">
							<div class="carousel-item active">
								<img src="img/paisaje1.jpg" alt="paisaje1">
							</div>
							<div class="carousel-item">
								<img src="img/paisaje2.jpg" alt="paisaje2">
							</div>
							<div class="carousel-item">
								<img src="img/paisaje3.jpg" alt="paisaje3">
							</div>
						</div>
					
						<!-- Control de Derecha e Izquierda -->
						<a class="carousel-control-prev" href="#demo" data-slide="prev">
							<span class="carousel-control-prev-icon"></span>
						</a>
						<a class="carousel-control-next" href="#demo" data-slide="next">
							<span class="carousel-control-next-icon"></span>
						</a>
					
				</div>  
		</div>
</head>

<!--Inicio del Cuerpo de la Pagina-->
<body >

	<!--Inicio del Contenedor-->
		<div class="container">
		<form method="post" action="registrado.html">
			<br>

			<!--Campo donde se ingresara el Rut-->
			<div class="form-group row " >
				<div class="col-sm-2" >
					<label for="inputEmail3" class="col-sm-2 col-form-label ">Rut:</label>
				</div>
				<div class="col-sm-10 align-self-center">
				  <input type="text" id="rut" class="form-control" name="rut" required oninput="checkRut(this)" placeholder="Ingrese RUT" maxlength="12s">
				</div>
			</div>
			<!--Campo para Ingresar los Nombres-->
			<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">Nombres:</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ingrese Nombre" minlength="3" maxlength="100">
				</div>
			</div>
			<!--Campo para ingresar los Apellidos-->
			<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">Apellidos:</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Ingrese Apellidos" minlength="3" maxlength="100">
				</div>
			</div>
			<!--Campo donde se Selecciona la fecha de Nacimiento-->
			<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label ">Fecha de Nacimiento:</label>
				<div class="col-sm-10">
				  <input type="date" class="form-control" name="nacimiento"  id="nacimiento" placeholder="" onchange="getObject(this);" >
				</div>
			</div>
			<!--Campo donde se imprime la Edad del usuario que ingreso su fecha de nacimiento-->
			<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">Edad:</label>
				<div class="col-sm-10">
				  <input type="number" class="form-control" name="edad" id="edad" placeholder="Su Edad" disabled>
				</div>
			</div>
			<!--Campo donde se ingresa el Correo electronico-->
			<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">Correo Electronico:</label>
				<div class="col-sm-10">
				  <input type="email" class="form-control" name="email" id="email" placeholder="Ingrese su Correo Electronico">
				</div>
			</div>
			<!--Campo donde se selecciona una Región-->
			<div class="form-group row">
					<label for="imputEmail3" class="col-sm-2 col-form-label">Región: </label>
			<div class="col-sm-10">
				<select class="custom-select" name="region" id="region">
					<option selected value="0" >-- Seleccione --</option>
				</select>
			</div>	
			</div>
			<!--Campo donde se Selecciona una provincia de de la Region seleccionada-->
			<div class="form-group row">
					<label for="imputEmail3" class="col-sm-2 col-form-label">Provincia: </label>
			<div class="col-sm-10">
				<select class="custom-select" name="provincia" id="provincia">
					<option selected value="0" >-- Seleccione --</option>
				</select>
			</div>	
			</div>
			<!--Campo donde se Selecciona una Comuna de la Provincia Seleccionada-->
			<div class="form-group row">
					<label for="imputEmail3" class="col-sm-2 col-form-label">Comuna: </label>
			<div class="col-sm-10">
				<select class="custom-select" name="comuna" id="comuna">
					<option selected value="0" >-- Seleccione --</option>
				</select>
			</div>	
			</div>

			<!--Campo donde se crea el boton para enviar los datos ingresados-->
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >
			  Enviar
			</button>			
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document"> 
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <!--Pantalla desplegada al seleccionar el boton enviar, donde pregunta si desea guardar los cambios-->
			      <div class="modal-body">
			        Esta saliendo de la pagina, ¿desea guardar los cambios?
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
			        <button type="submit" class="btn btn-primary" id="guardar">Guardar Cambios</button>
			      </div>
			    </div>
			  </div>
			</div>
			<br>
			<br>

			<footer>
				<link href='https://fonts.googleapis.com/css?family=ABeeZee' rel='stylesheet'>
				<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
				<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
			</footer>

			</form>
		<br>
	</div>
				<br>
			<br>
						<br>

		</body>
</html>