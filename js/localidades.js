var Regiones = [
    { "id_region": "05", "nombre": "Valparaiso" },
    { "id_region": "08", "nombre": "Biobío" },
    { "id_region": "10", "nombre": "Los Lagos" },
]

var Provincias = [
    
    { "id_region": "05", "id_provincia": "051", "nombre": "Valparaiso" },
    { "id_region": "05", "id_provincia": "054", "nombre": "Petorca" },
    { "id_region": "05", "id_provincia": "055", "nombre": "Quillota" },

    { "id_region": "08", "id_provincia": "081", "nombre": "Concepcion" },
    { "id_region": "08", "id_provincia": "082", "nombre": "Arauco" },
    { "id_region": "08", "id_provincia": "083", "nombre": "Biobío" },
    
    { "id_region": "10", "id_provincia": "101", "nombre": "Llanquihue" },
    { "id_region": "10", "id_provincia": "102", "nombre": "chiloé" },
    { "id_region": "10", "id_provincia": "103", "nombre": "Osorno" }
    
]

var Comunas = [
    { "id_region": "05", "id_provincia": "051","id_comuna": "05102", "nombre": "Casablanca" },
    { "id_region": "05", "id_provincia": "051","id_comuna": "05107", "nombre": "Quintero" },
    
    { "id_region": "05", "id_provincia": "054","id_comuna": "05401", "nombre": "La Ligua" },
    { "id_region": "05", "id_provincia": "054","id_comuna": "05403", "nombre": "Papudo" },
    
    { "id_region": "05", "id_provincia": "055","id_comuna": "05502", "nombre": "Calera" },
    { "id_region": "05", "id_provincia": "055","id_comuna": "05504", "nombre": "La Cruz" },

    
    { "id_region": "03", "id_provincia": "081","id_comuna": "08104", "nombre": "Florida" },
    { "id_region": "03", "id_provincia": "081","id_comuna": "08108", "nombre": "San Pedro de la Paz" },
    
    { "id_region": "03", "id_provincia": "082","id_comuna": "08202", "nombre": "Arauco" },
    { "id_region": "03", "id_provincia": "082","id_comuna": "08206", "nombre": "Los Alamos" },
    
    { "id_region": "03", "id_provincia": "083","id_comuna": "08301", "nombre": "Los Angeles" },
    { "id_region": "03", "id_provincia": "083","id_comuna": "08311", "nombre": "Santa Bárbara" },

    
    { "id_region": "10", "id_provincia": "101","id_comuna": "10101", "nombre": "Puerto Montt" },
    { "id_region": "10", "id_provincia": "101","id_comuna": "10105", "nombre": "Frutillar" },
    
    { "id_region": "10", "id_provincia": "102","id_comuna": "10202", "nombre": "Ancud" },
    { "id_region": "10", "id_provincia": "102","id_comuna": "10207", "nombre": "Queilen" },
    
    { "id_region": "10", "id_provincia": "103","id_comuna": "10304", "nombre": "Puyehue" },
    { "id_region": "10", "id_provincia": "103","id_comuna": "10305", "nombre": "Río Negro" },
]
